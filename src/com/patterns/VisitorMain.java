package com.patterns;

import com.patterns.shapes.Circle;
import com.patterns.shapes.ComplexShape;
import com.patterns.shapes.Point;
import com.patterns.shapes.Rectangle;
import com.patterns.visitors.ConcreteVisitor;

import java.util.List;

public class VisitorMain {
    public static void main(String[] args) {
        var p = new Point(10, 20);
        p.draw();
        var c = new Circle(10, 20, 30);
        c.draw();
        var r = new Rectangle(10, 20, 40, 50);
        r.draw();
        var complexShape = new ComplexShape();
        complexShape.addShape(List.of(new Point(1, 1), new Circle(2, 2, 2), new Rectangle(3, 3, 3, 3)));
        complexShape.draw();
        System.out.println();
        System.out.println();
        System.out.println();
        var concreteVisitor = new ConcreteVisitor();
        concreteVisitor.export(p, c, r, complexShape);
    }
}
