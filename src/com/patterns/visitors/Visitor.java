package com.patterns.visitors;

import com.patterns.shapes.Circle;
import com.patterns.shapes.ComplexShape;
import com.patterns.shapes.Point;
import com.patterns.shapes.Rectangle;

public interface Visitor {
    void visitPoint(Point p);

    void visitRectangle(Rectangle r);

    void visitCircle(Circle c);

    void visitComplexShape(ComplexShape c);
}
