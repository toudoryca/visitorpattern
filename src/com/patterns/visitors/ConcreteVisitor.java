package com.patterns.visitors;

import com.patterns.shapes.*;

public class ConcreteVisitor implements Visitor {
    @Override
    public void visitPoint(Point p) {
        System.out.println("Pvisit "+p.toString());
    }

    @Override
    public void visitRectangle(Rectangle r) {
        System.out.println("Rvisit "+r.toString());

    }

    @Override
    public void visitCircle(Circle c) {
        System.out.println("Cvisit "+c.toString());

    }

    @Override
    public void visitComplexShape(ComplexShape c) {
        System.out.println("visit "+c.toString());
        for (var shape: c.getShapes()
             ) {
            shape.accept(this);
        }
        System.out.println("end visit ComplexShape");
    }

    public void export(Shape... shapes){
        for (var shape :
                shapes) {
            shape.accept(this);

        }
    }
}
