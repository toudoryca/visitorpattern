package com.patterns.shapes;

import com.patterns.visitors.Visitor;

public class Point extends Shape {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point() {

    }

    public int getX() {

        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void draw() {
        System.out.println("draw point " + toString());
        ;
    }

    @Override
    public void accept(Visitor v) {
        v.visitPoint(this);
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public String getPoint() {
        return "x=" + x +
                ", y=" + y;
    }
}
