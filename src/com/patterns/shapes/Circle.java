package com.patterns.shapes;

import com.patterns.visitors.Visitor;

public class Circle extends Point {
    int radius;

    public Circle(int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }

    public Circle() {

    }

    public int getRadius() {

        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle{" + getPoint() +
                ", radius=" + radius +
                '}';
    }

    @Override
    public void draw() {
        System.out.println("draw circle " + toString());
        ;
    }

    @Override
    public void accept(Visitor v) {
        v.visitCircle(this);
    }
}
