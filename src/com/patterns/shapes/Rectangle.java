package com.patterns.shapes;

import com.patterns.visitors.Visitor;

public class Rectangle extends Point {
    int height;
    int width;

    public Rectangle(int x, int y, int height, int width) {
        super(x, y);
        this.height = height;
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public void draw() {
        System.out.println("draw rectangle " + toString());
        ;
    }

    @Override
    public String toString() {
        return "Rectangle{" + getPoint() +
                ", height=" + height +
                ", width=" + width +
                '}';
    }

    @Override
    public void accept(Visitor v) {
        v.visitRectangle(this);
    }
}
