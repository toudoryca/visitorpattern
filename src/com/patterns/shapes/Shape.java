package com.patterns.shapes;

import com.patterns.visitors.Visitor;

public abstract class Shape {
    public abstract void draw();

    public abstract void accept(Visitor v);
}
