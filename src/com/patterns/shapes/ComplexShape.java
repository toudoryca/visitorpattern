package com.patterns.shapes;

import com.patterns.visitors.Visitor;

import java.util.ArrayList;
import java.util.List;

public class ComplexShape extends Shape {
    ArrayList shapes = new ArrayList<Shape>();

    public ArrayList<Shape> getShapes() {
        return shapes;
    }

    public ComplexShape() {
    }

    public void addShape(List s) {
        shapes.addAll(s);
    }

    @Override
    public void draw() {
        System.out.println("draw complex Shape " + toString());
    }

    @Override
    public String toString() {
        return "ComplexShape{" +
                "shapes=" + shapes +
                '}';
    }

    @Override
    public void accept(Visitor v) {
        v.visitComplexShape(this);
    }
}
